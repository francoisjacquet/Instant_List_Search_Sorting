# Instant List Search and Sorting plugin

![](https://gitlab.com/francoisjacquet/Instant_List_Search_Sorting/raw/main/video.mp4)

https://gitlab.com/francoisjacquet/Instant_List_Search_Sorting/

Version 1.4 - July, 2024

Author François Jacquet

Sponsored by AT group, Slovenia

License GNU/GPLv2 or later

## Description

This RosarioSIS plugin modifies the way lists are searched and sorted.

By default, RosarioSIS reloads the page when you click on a list column to sort it, or when you search the list.

With this plugin, the list rows are instantly sorted and searched without reloading the page.

Note: this applies to every list and every user in the system.

Note 2: this plugin may later be included in core RosarioSIS.


## Content

None

## Install

Copy the `Instant_List_Search_Sorting/` folder (if named `Instant_List_Search_Sorting-main`, rename it) and its content inside the `plugins/` folder of RosarioSIS.

Go to _School > Configuration > Plugins_ and click "Activate".

Reload the page (`F5` key) to load the plugin.

Requires RosarioSIS 11.6+
