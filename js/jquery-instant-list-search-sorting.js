/**
 * jQuery Instant List Search and Sorting
 *
 * TODO before include in core:
 * Make PHP (unless PDF or export) search equal to JS search,
 * that is only hide rows not matching, do not remove them
 * and remove "Relevance" column
 * and use the same regex
 *
 * @package Instant List Search and Sorting plugin
 */

var oldAjaxPrepare = ajaxPrepare;

var ajaxPrepare = function(target, scrollTop) {
	oldAjaxPrepare(target, scrollTop);

	if (target == '#body' || target == 'body') {
		instantListSearch();
		instantListSorting();
	}
}

/**
 * jQuery Instant List Search
 *
 * 0. On search input keyup (after 400ms)
 * 1. Remove repeated thead
 * 2. Show all rows & enable inputs
 * 3. Search rows (inner text, case insensitive)
 * 4. Hide not matching rows (except for "Add" row)
 * 5. Repeat thead
 * 6. Update export list link href, th sort link href
 * 7. Update history URL, bottom back button URL
 *
 * @link https://stackoverflow.com/questions/9127498/how-to-perform-a-real-time-search-and-filter-on-a-html-table
 */
var instantListSearch = function() {
	/**
	 * Returns a function, that, as long as it continues to be invoked, will not be triggered.
	 * The function will be called after it stops being called for N milliseconds.
	 *
	 * @link https://davidwalsh.name/javascript-debounce-function
	 */
	var debounce = function(func, wait) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				func.apply(context, args);
			};
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
		};
	};

	$('.list').each(function() {
		var $list = $(this),
			$searchInput = $list.parent('.list-wrapper').prev('.list-nav').find('#LO_search');

		if (! $searchInput.length) {
			return;
		}

		// Deactivate old search method.
		$searchInput[0].onkeypress = '';
		$searchInput.next('.button')[0].onclick = '';
		$searchInput.next('.button').css('cursor', 'default');

		// Get rows inside the list table body.
		// List may contain nested arrays, so we use children() and not find().
		var $rows = $list.children('tbody').children('tr');

		if (! $rows.length) {
			return;
		}

		var valTmp = $searchInput.val();

		$searchInput.on('keyup', debounce(function() {
		$searchInput.on('keyup paste cut', debounce(function() {
			var val = this.value,
				escapedRegExp = val.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'), // $& means the whole matched string
				pattern = '^(?=.*\\b' + $.trim(escapedRegExp).split(/\s+/).join('.*\\b)(?=.*\\b') + ').*$',
				reg = RegExp(pattern, 'i');

			if (val === valTmp) {
				// Prevent searching again when browser tab gains focus (keyup event fired)
				return;
			}

			valTmp = val;

			$list.find('tr.thead-repeat').remove();

			$rows.show().find('input,select,textarea').attr('disabled', false);

			if (val) {
				var addRowInd = $list.find('tr.list-add-row').index();

				$rows.filter(function(i) {
					if (addRowInd == i) {
						return false;
					}

					return !reg.test(this.innerText.replace(/\s+/g, ' '));
				}).hide().find('input,select,textarea').attr('disabled', true);
			}

			instantListRepeatTHead($list);

			var $save = $searchInput.parents('.list-nav').find('.list-save'),
				loSearch = '&LO_search=' + encodeURIComponent(val);

			if ($save.length) {
				// Update Export list link href: &LO_search
				$save[0].href = instantListSetURLParam($save[0].href, 'LO_search', val);
			}

			$list.find('th a').each(function() {
				// Update th sort link href: &LO_search
				this.href = instantListSetURLParam(this.href, 'LO_search', val);
			});

			var url = document.URL,
				listId = $list.data('list-id');

			url = instantListSetURLParam(url, 'LO_search', val);

			if (listId) {
				url = instantListSetURLParam(url, 'LO_id', listId);
			}

			// Update history URL
			history.replaceState({}, '', url);

			if ($('#BottomButtonBack').length
				&& $('#BottomButtonBack').attr('href').indexOf(instantListGetURLParam(url, 'modname')) > 0) {
				// Update bottom back button URL
				$('#BottomButtonBack').attr('href', url);
			}
		}, 400));
	});
}

/**
 * jQuery Instant List Sorting
 *
 * 0. On th link click
 * 1. Remove repeated thead
 * 2. Sort rows (inner text or HTML comment, numeric or alphabetic, ascending or desending)
 * 3. Do not sort "Add" row so it stays at the same place
 * 4. Repeat thead
 * 5. Update th link href: &LO_dir, Export list link href: &LO_dir, &LO_sort
 * 6. Update history URL, bottom back button URL
 *
 * @link https://stackoverflow.com/questions/3160277/jquery-table-sort
 */
var instantListSorting = function() {
	$('.list').each(function() {
		var $list = $(this);

		$list.find('th a').on('click', function(e) {
			e.preventDefault();
			e.stopPropagation();

			$list.find('tr.thead-repeat').remove();

			var $addRow = $list.find('tr.list-add-row'),
				addRowInd = $addRow.index();

			if (addRowInd >= 0) {
				$addRow.remove();
			}

			var th = e.target.parentNode,
				rows = $list.children('tbody').children('tr').toArray().sort(
					comparer($(th).index())
				);

			th.asc = !th.asc;

			if (!th.asc) {
				rows = rows.reverse();
			}

			$list.append(rows);

			if (!addRowInd) {
				$list.prepend($addRow);
			} else if (addRowInd > 0) {
				$list.append($addRow);
			}

			instantListRepeatTHead($list);

			// Update history URL
			history.replaceState({}, '', e.target.href);

			if ($('#BottomButtonBack').length
				&& $('#BottomButtonBack').attr('href').indexOf(instantListGetURLParam(e.target.href, 'modname')) > 0) {
				// Update bottom back button URL
				$('#BottomButtonBack').attr('href', e.target.href);
			}

			var dir = th.asc ? 1 : -1;

			// Update th link href: &LO_dir
			e.target.href = instantListSetURLParam(e.target.href, 'LO_dir', (dir * -1));

			var $save = $list.parents('.list-outer').find('.list-save');

			if ($save.length) {
				// Update Export list link href: &LO_dir, &LO_sort
				$save[0].href = instantListSetURLParam($save[0].href, 'LO_dir', dir);

				$save[0].href = instantListSetURLParam(
					$save[0].href,
					'LO_sort',
					instantListGetURLParam(e.target.href, 'LO_sort')
				);
			}
		});
	});

	var comparer = function(i) {
		return function(a, b) {
			var valA = getCellVal(a, i),
				valB = getCellVal(b, i);

			return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB);
		}
	};

	var getCellVal = function(row, i) {
		var cell = row.children[i];

		if (cell.firstChild
			&& cell.firstChild.nodeName === '#comment') {
			// Extract sort HTML comment.
			return cell.firstChild.data.trim();
		}

		if (!cell.innerText) {
			return cell.innerHTML.trim();
		}

		return cell.innerText.trim();
	};
}

// @link https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
// @deprecated since RosarioSIS 12.0 use getURLParam()
function instantListGetURLParam(url, name) {
	name = name.replace(/[\[\]]/g, '\\$&');
	var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
		results = regex.exec(url);
	if (!results || !results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

// @deprecated since RosarioSIS 12.0 use setURLParam()
function instantListSetURLParam(url, name, value) {
	name = encodeURIComponent(name);
	var newParam = name + '=' + encodeURIComponent(value),
		sep = url.indexOf('&' + name + '=') > 0 ? '&' :
			(url.indexOf('?' + name + '=') >= 0 ? '?' : '');

	if (!sep) return url + (url.indexOf('?') >= 0 ? '&' : '?') + newParam;

	return url.replace(
		sep + name + '=' + encodeURIComponent(instantListGetURLParam(url, name)),
		sep + newParam
	);
}


// Repeat long list table header.
// @dperecated since RosarioSIS 12.0 use repeatListTHead()
var instantListRepeatTHead = function($lists) {
	if (!$lists.length)
		return;

	$lists.each(function(i, tbl) {
		var trs = $(tbl).children("thead,tbody").children("tr:visible"), // Here, we add `:visible`
			tr_num = trs.length,
			tr_max = 20;

		// If more than 20 rows.
		if (tr_num > tr_max) {
			var th = trs[0];

			// Each 20 rows, or at the end if number of rows <= 40.
			for (var j = (tr_num > tr_max * 2 ? tr_max : tr_num - 1), trs2th = []; j < tr_num; j += tr_max) {
				var tr = trs[j];
				trs2th.push(tr);
			}

			// Clone header.
			$(th).clone().addClass('thead-repeat').insertAfter(trs2th);
		}
	});
}
