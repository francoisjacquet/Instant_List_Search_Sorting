<?php
/**
 * Functions.
 *
 * @package Instant List Search and Sorting plugin
 */

add_action( 'Warehouse.php|header_head', 'InstantListSearchSortingJS' );

// Load our JS once, in <head>
function InstantListSearchSortingJS()
{
	// Load JS if not already in core.
	?>
	<script>
		if (typeof instantListSearch === 'undefined') {
			var instantListSearchSortingJS = document.createElement('script');
			instantListSearchSortingJS.src = 'plugins/Instant_List_Search_Sorting/js/jquery-instant-list-search-sorting.js';
			document.head.appendChild(instantListSearchSortingJS);
		}
	</script>
	<?php
}
